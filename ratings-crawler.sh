#!/bin/bash
source ./crawler-env

CMD="pipenv run scrapy crawl ratings-spider"

echo "Processing list: '${STATE_LIST}'"

for state in ${STATE_LIST}
do
   echo "### -------------- ${state} --------------- ###"
   DATA_FILE="${DATA_DIR}/${state}-routes.jsonlines"
   if [[ -f ${DATA_FILE} ]]
   then
      $CMD -L INFO -o ${state}-ratings.jsonlines -a "f=$DATA_FILE"
   else
      echo "Data file not found: ${DATA_FILE}"
   fi
done
