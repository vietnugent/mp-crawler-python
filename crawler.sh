#!/bin/bash
for state in $STATE_LIST
do
   echo "### -------------- ${state} --------------- ###"
   pipenv run scrapy crawl state-spider -s JOBDIR=job-dir/${state}/a -L INFO -o ${state}-areas.jsonlines -a "state=${state}"
   pipenv run scrapy crawl crag-spider -s JOBDIR=job-dir/${state}/r -L INFO -o ${state}-routes.jsonlines -a "f=${state}-areas.jsonlines"
done
