## MountainProject crawler

### Setup your environment

1.  Install Python 3.7 and pipenv (pip3 install pipenv)

2.  In the project root directory: `pipenv install --skip-lock`

3.  Enter pipenv shell: `pipenv shell`

4.  Test your env
```
crapy shell https://google.com
# You will see a wall of text
# Print the response object
# >>> print(response)
# >>> exit()
```

### How to run

Crawl responsibly by identifying yourself (and your website) on the user-agent.  Open `mp/settings` and uncomment/edit `USER_AGENT = '< your website >'`.

Get routes and areas:

```bash
# Define a space-separated-list of states
export STATE_LIST="nv"
./crawler.sh
```

### License

MIT
