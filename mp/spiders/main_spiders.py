import scrapy


class MainSpider(scrapy.Spider):
    name = "dirtbag-joe"

    start_urls = ['https://www.mountainproject.com/']

    def parse(self, response):
        for area in response.css('#route-guide strong a'):
            yield {
                'url': area.css('::attr(href)').extract_first(),
                'area_name': area.css('::text').extract_first(),
            }