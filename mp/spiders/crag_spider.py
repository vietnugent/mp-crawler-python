import scrapy
import urllib.parse
import logging
import jsonlines
import re
from functools import reduce

TYPES = {
    "sport": False,
    "trad": False,
    "tr": False,
    "aid": False,
    "mixed": False,
    "ice": False,
    "snow": False,
    "alpine": False,
    "boulder": False
}

SAFETY_TYPES = (
    "R", "PG", "PG13", "X"
)

AID_REGEX_PATTERN = re.compile(r'^A[0-5][\+\-]?|C[0-5][\+\-]?')


class CragSpider(scrapy.Spider):
    name = "crag-spider"

    us_state = None
    crag_id = None

    def __init__(self, *args, **kwargs):
        super(CragSpider, self).__init__(*args, **kwargs)
        self.intputFile = kwargs.get('f')
        self.crag_id = kwargs.get('cid', '')
        # if self.crag_id is not None:
        #     self.start_urls =  ["https://www.mountainproject.com/area/" + self.crag_id]
        #     return
        # else:
        #     self.start_urls = []

    def start_requests(self):

        with jsonlines.open(self.intputFile, mode="r") as r:
            for obj in r.iter(type=dict, skip_invalid=True):
                if obj["url"].find(self.crag_id) == -1:
                    continue

                obj["sector_id"] = self.extract_id_from(obj["url"], "area")
                request = scrapy.Request(
                    obj["url"], callback=self.parse, meta=obj)
                yield request

    def parse(self, response):
        # Process a single crag
        # Get all routes on the left nav box
        rows = response.css('[id="left-nav-route-table"] tr')

        logging.info("=== Crawling crag {}, found {} climbs ===".format(
            response.meta["area_name"], len(rows)))

        for row in rows:
            # extract left-to-right sort order
            lr_seq = row.css("::attr('data-lr')").get().strip()
            response.meta["lr_seq"] = lr_seq
            #  the a tag
            a_tag = row.css("a")
            if len(a_tag.getall()) == 0:
                # we're at the "unsorted" section header so a tag doesn't exist.
                continue
            request = scrapy.Request(a_tag.css('::attr(href)').extract_first(
            ), callback=self.parse_route, meta=response.meta)
            yield request

    def parse_route(self, response):
        title = response.css(
            "div.pt-main-content h1::text").extract_first().strip()

        if not title:
            title = "Unknown"

        meta = response.css("table.description-details tr")
        raw_type = meta[0].css("td::text").getall()[1].strip()
        fa = meta[1].css("td::text").getall()

        types = self.parse_types(raw_type)
        grade = {}
        safety = ""
        if self.is_rock(types) or self.is_boulder:
            raw_grades = response.css("h2.mr-2 span::text").getall()
            grade = self.parse_grades(raw_grades)
            extras = "".join(response.css("h2.mr-2::text").getall()).strip()
            if (self.is_rock(types)):
                safety = self.parse_safety(extras)
            if (types.get("aid")):
                aid_grade = self.parse_aid(extras, safety)
                grade["yds_aid"] = aid_grade

        elif self.is_ice(types):
            grade = response.css("h2.mr-2::text").get()

        # Get Description, Location and Protection
        details = response.css("div.mt-2")
        x = {
            "description": "",
            "location": "",
            "protection": ""
        }
        # TODO need to comment/explain this section
        # I can't understand my own code after a few months
        for d in details:
            _key = key = d.css("h2::text").get()
            if _key is None:
                continue
            key = _key.strip().lower()
            lines = d.css("div.fr-view ::text").getall()
            # getall() call above will convert <br> to empty lines, a tags to separate elements
            x[key] = reduce(CragSpider.concat, lines, [' '])

        yield {
            'route_name': title,
            'grade': grade,
            'safety': safety,
            'type': types,
            'fa': fa[1].strip(),
            'description': self.safe_details(x, 'description'),
            'location': self.safe_details(x, 'location'),
            'protection': self.safe_details(x, 'protection'),
            'metadata': {
                'left_right_seq': response.meta["lr_seq"],
                'parent_lnglat': response.meta["lnglat"],
                'parent_sector': response.meta["area_name"],
                'mp_route_id': self.extract_id_from(response.request.url, "route"),
                'mp_sector_id': response.meta["sector_id"],
                'mp_path': response.meta["path"]
            }
        }

    @staticmethod
    def parse_types(raw):
        raw_types = raw.split(",")
        result = {}
        for t in raw_types:
            key = t.lower().strip()
            if key in TYPES:
                result[key] = True
        return result

    @staticmethod
    def parse_grades(grades):
        x = {
        }
        if (not grades):
            return x

        for i in range(1, 12, 2):
            if i > len(grades):
                break
            x[grades[i]] = grades[i-1].strip()
        return x

    @staticmethod
    def safe_details(dict, key):
        if key in dict:
            return dict[key]
        else:
            return ""

    @staticmethod
    def parse_safety(extras):
        for t in SAFETY_TYPES:
            if extras.find(t) > -1:
                return t
        return ""

    @staticmethod
    def parse_aid(extras, safety):
        s = extras.replace(safety, '')
        if (AID_REGEX_PATTERN.match(s)):
            return s
        return ""

    @staticmethod
    def parse_gps(response):
        # extract lat long from google maps url
        google_url = response.selector. \
            xpath(
                '//table[@class="description-details"]//a[starts-with(@href, "http://maps.google.com")]/@href').extract_first()
        return CragSpider.parse_lnglat(google_url)

    @staticmethod
    def parse_lnglat(url):
        if url is None:
            return None
        obj = dict(urllib.parse.parse_qsl(urllib.parse.urlsplit(url).query))
        if 'q' in obj:
            return obj['q']
        return None

    @staticmethod
    def extract_id_from(url, type):
        x = re.findall(r'{}\/\d+'.format(type), url)  # TODO: use capture group
        if len(x):
            z = x[0].split("/")
            if len(z):
                return z[1]
        return -1

    @staticmethod
    def is_rock(types):
        if types.get("trad") or types.get("sport") or types.get("tr") or types.get("aid"):
            return True
        return False

    @staticmethod
    def is_ice(types):
        if types.get("ice") or types.get("mixed") or types.get("snow") or types.get("alpine"):
            return True
        return False

    @staticmethod
    def is_boulder(types):
        if (types.get("boulder")):
            return True
        return False

    # To be used with `reduce`.  Concat all elements until empty blanks.  Start a new line and repeat
    @staticmethod
    def concat(acc, current):
        if (current.strip()):
            last = acc.pop()
            if (last == ' '):
                acc.append(current)
            else:
                acc.append(last + current)
            return acc
        lastIndex = len(acc) - 1
        if (acc[lastIndex] != ' '):
            acc.append(' ')
        return acc
