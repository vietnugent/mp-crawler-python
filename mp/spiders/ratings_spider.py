import scrapy
import urllib.parse
import logging
from scrapy.spiders import CrawlSpider
import jsonlines
import re
import functools

class RatingsSpider(CrawlSpider):
    name = "ratings-spider"

    us_state = None
    crag_id = None

    def __init__(self, *args, **kwargs):
        super(RatingsSpider, self).__init__(*args, **kwargs)
        self.intputFile = kwargs.get("f")
        self.search_route_id = kwargs.get("rid", "")

    def start_requests(self):
        with jsonlines.open(self.intputFile, mode="r") as r:
            for obj in r.iter(type=dict, skip_invalid=True):
                route_id = obj["metadata"]["mp_route_id"]
                types = obj["type"]

                # TODO: why only trad & sport?
                if types.get("trad") or types.get("sport"):
                    url = "https://www.mountainproject.com/route/stats/{}".format(
                        route_id
                    )
                    request = scrapy.Request(url, callback=self.parse, meta=obj)
                    yield request

    def parse(self, response):
        # Process a single crag

        # Get all ratings
        rows = response.xpath(
            "//h3[contains(., 'Star Ratings')]/following-sibling::table/tr"
        )

        logging.info(
            "=== Parse {}: {} ratings ===".format(
                response.meta["route_name"], len(rows)
            )
        )
        users = []
        ratings = []
        for row in rows:
            user_url = row.css("a::attr(href)").extract_first()
            user_id = RatingsSpider.extract_id_from(user_url, "user")
            stars = len(row.css("span.scoreStars img").getall())
            ratings.append(stars)
            users.append(user_id)
            # logging.info("{}: {}".format(user_id, stars))

        yield {
            "route_id": response.meta["metadata"]["mp_route_id"],
            "name": response.meta["route_name"],
            "grade": response.meta["grade"].get("YDS", ""),
            "type": response.meta["type"],
            "users": users,
            "ratings": ratings,
        }

    @staticmethod
    def extract_id_from(url, type):
        x = re.findall(r"{}\/\d+\/".format(type), url)
        if len(x):
            z = x[0].split("/")
            if len(z):
                return z[1]
        return ""