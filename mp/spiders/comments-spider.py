import scrapy
from scrapy.spiders import CrawlSpider
import jsonlines


class RatingsSpider(CrawlSpider):
    name = "comments-spider"

    def __init__(self, *args, **kwargs):
        super(RatingsSpider, self).__init__(*args, **kwargs)
        self.intputFile = kwargs.get("f")

    def start_requests(self):
        with jsonlines.open(self.intputFile, mode="r") as r:
            for obj in r.iter(type=dict, skip_invalid=True):
                route_id = obj["metadata"]["mp_route_id"]
                url = "https://www.mountainproject.com/Climb-Route/{}/comments?sortOrder=oldest&showAll=true".format(
                    route_id)
                request = scrapy.Request(url, callback=self.parse, meta=obj)
                yield request

    def parse(self, response):
        rows = response.css("table.main-comment")

        comments = []
        for row in rows:
            node = row.css("div.comment-body")

            s_str = node.css('span[id*="-full"]::text').getall()

            comments.append({
                "text": [line.strip() for line in s_str if line.strip()],
                "beta":
                row.css('div.like span.num-likes::text').get(),
                "date":
                node.css('span.comment-time>a::text').get()
            })

        yield {
            "route_id": response.meta["metadata"]["mp_route_id"],
            "name": response.meta["route_name"],
            "grade": response.meta["grade"].get("YDS", ""),
            "type": response.meta["type"],
            "comments": comments,
        }