import scrapy
import urllib.parse
import logging
import scrapy
from functools import reduce

from .us_state_abbrev import abbrev_us_state

from .crag_spider import CragSpider


class AreaSpider(scrapy.Spider):
    name = "state-spider"

    start_urls = ['https://www.mountainproject.com']

    us_state = None

    def __init__(self, *args, **kwargs):
        super(AreaSpider, self).__init__(*args, **kwargs)
        s = kwargs.get('state')
        if (s):
            state = abbrev_us_state[s.upper()]
            self.us_state = state

    def parse(self, response):
        if self.us_state is not None:
            # Process a single US State
            logging.info(
                "#### Getting specific US state {} ####".format(self.us_state))
            a_tag = response.xpath(
                '//div[@id="route-guide"]//a[contains(., $us_state)]', us_state=self.us_state)
            request = scrapy.Request(
                a_tag.css('::attr(href)').extract_first(), callback=self.parse_area)
            request.meta['us_state'] = self.us_state
            yield request

    def parse_area(self, response):
        lnglat_from_parent = False
        header = response.css('div.mp-sidebar h3::text').extract_first()

        if header is None or header.startswith('Routes'):

            # we reach Area "leaf" node where all Routes are listed
            lnglat = AreaSpider.parse_gps(response)
            if lnglat is None:
                lnglat_from_parent = True
                lnglat = response.meta['parent_lnglat']
                logging.warning("No GPS found for {}. Using parent's: {}".
                                format(response.meta['area_name'], lnglat))

            path = response.meta['area_path']
            response.meta['area_path'] = None

            # Get Description, Location
            descNode = response.css("h2.mt-2 + div.fr-view ::text").getall()
            locationNode = response.css(
                "div.mt-2 > div.fr-view ::text").getall()

            yield {
                'area_name': response.meta['area_name'],
                'description': reduce(CragSpider.concat, descNode, [' ']),
                'location': reduce(CragSpider.concat, locationNode, [' ']),
                'path': path,
                'us_state': response.meta['us_state'],
                'url': response.request.url,
                'lnglat': lnglat,
                'metadata': {
                    'lnglat_from_parent': lnglat_from_parent,

                }
            }

        elif header.startswith('Areas'):
            # For each area on the left nav we recursively navigate to each one
            # Pass selected info about current area to children via request.meta
            for a_tag in response.css('div.mp-sidebar div.lef-nav-row a'):
                # Remember that scrapy.Request(..) won't get executed until yield request at the end
                request = scrapy.Request(
                    a_tag.css('::attr(href)').extract_first(), callback=self.parse_area)

                request.meta['us_state'] = response.meta['us_state']

                area_name = a_tag.css('::text').extract_first()
                request.meta['area_name'] = area_name

                # Build the path to this area and pass it down to children
                path = response.meta.get('area_path')
                if not path:
                    request.meta['area_path'] = area_name
                else:
                    path = path + "|" + area_name
                    request.meta['area_path'] = path

                # pass the area's lnglat down to children
                area_lnglat = AreaSpider.parse_gps(response)
                if area_lnglat is None:
                    # we don't even have it! hopefully our ancestors do
                    area_lnglat = response.meta['parent_lnglat']
                request.meta['parent_lnglat'] = area_lnglat
                yield request

        else:
            logging.warning("Unknown header text: {}".format(header))

    @staticmethod
    def parse_gps(response):
        # extract lat long from google maps url
        google_url = response.selector. \
            xpath(
                '//table[@class="description-details"]//a[starts-with(@href, "http://maps.google.com")]/@href').extract_first()
        return AreaSpider.parse_lnglat(google_url)

    @staticmethod
    def parse_lnglat(url):
        if url is None:
            return None
        obj = dict(urllib.parse.parse_qsl(urllib.parse.urlsplit(url).query))
        lnglat = obj['q'].split(',')
        if 'q' in obj:
            return [float(lnglat[1]), float(lnglat[0])]
        return None
